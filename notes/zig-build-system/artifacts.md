# Artifacts

An artifact seems to be a some sort of build artifact or compile artifact, this is what I have
gathered from writing build scripts for some time as artifacts are of type `*std.Compile` which are
returned by functions such as `b.addExecutable` and `b.addStaticLibrary`.

This means when you see functions that have artifact in the name such as `b.addRunArtifact` it
generally means you pass it the exe or lib in terms of the default init files, although probably not
lib in this instance.

The `linkLibrary` function will take in an artifact, and you can get a dependency to return one of
it's artifacts with `b.dependency(...).artifact("artifact name")` allowing you to link it into your
program or libarary or whatever.
